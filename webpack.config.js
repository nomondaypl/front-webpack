var Encore = require("@symfony/webpack-encore");
var webpack = require('webpack');
var pjson = require('../../../package.json');

var devPath = (pjson.webpackPaths.app.devPath ? pjson.webpackPaths.app.devPath : "public/layout/default/dev");
var destPath = (pjson.webpackPaths.app.destPath ? pjson.webpackPaths.app.destPath : "public/layout/default/dist");
var publicPath = (pjson.webpackPaths.app.publicPath ? pjson.webpackPaths.app.publicPath : "/layout/default/dist");

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || "dev");
}

Encore.setOutputPath(destPath + "/")
    .setPublicPath(publicPath)
    .addEntry("app", "./" + devPath + "/js/app.js")
    // .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // .enableVersioning(Encore.isProduction())
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = "usage";
        config.corejs = 3;
    })
    .copyFiles([
        {
            from: "./" + devPath + "/gfx",
            to: "gfx/[path][name].[ext]",
        },
        {
            from: "./" + devPath + "/fonts",
            to: "fonts/[path][name].[ext]",
        },
        {
            from: "./" + devPath + "/other",
            to: "other/[path][name].[ext]",
        }
    ])
    .addPlugin(new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
    }))
    .enableSassLoader();
    // .enableIntegrityHashes(Encore.isProduction());

module.exports = Encore.getWebpackConfig();
